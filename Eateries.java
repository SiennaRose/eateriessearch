import javax.swing.*; 
import java.awt.event.*;
import java.awt.*;

import java.util.List; 
import realtimeweb.simplebusiness.SimpleBusiness; 
import realtimeweb.simplebusiness.domain.Business; 

/**
 * @author Sienna Sacramento
 * CS 108
 * Ms. Kraft
 * Dec. 12, 2017
 * 
 * This program uses an online API to access the yelp database and gives a list of businesses 
 * depending on the location and item given. 
 *
 */
public class Eateries 
{
	JTextField tf, tf1, tf3; 
	JButton b1, b2;  
	JTextArea textArea, ta1;
	List<Business> output; 
	
	/**
	 * <b>constructor</b>:calls setup()
	 */
	public Eateries()
	{
		setUp(); 
	}
	
	/**
	 * Returns the identification string of the program the author
	 * @return String id
	 */
	public String IdentificationString()
	{
		return "Program 8, Sienna Sacramento, Business: Eateries Search"; 
	}
	
	/**
	 * A basic selection sort taken from zybooks
	 * @param List<Business> output		a list of Businesses
	 * @param int numbersSize 			the size of the list
	 */
	public static void selectionSort(List<Business> output, int numbersSize) 
	{
	      int i = 0;
	      int j = 0;
	      int indexSmallest = 0;
	      Business temp = null;  // Temporary variable for swap

	      for (i = 0; i < numbersSize; ++i) {

	         // Find index of smallest remaining element
	         indexSmallest = i;
	         for (j = i + 1; j < numbersSize; ++j) {

	            if ((output.get(j)).getRating() < (output.get(indexSmallest)).getRating())
	            {
	               indexSmallest = j;
	            }
	         }

	         // Swap numbers[i] and numbers[indexSmallest]
	         temp = output.get(i);
	         output.set(i, output.get(indexSmallest));
	         output.set(indexSmallest, temp);
	      }
	}
	
	/**
	 * Retrives realtime online results from yelp of businesses in a certain area dependent on the 
	 * user's inputs
	 * 
	 * @param String item					user's input
	 * @param String location				user's input		
	 * @return List<Business> businesses	a list of resturants based off the online domain 
	 */
	public List<Business> onlineInfo(String item, String location)
	{
		SimpleBusiness yelp = new SimpleBusiness(); //ONLINE
		List<Business> businesses = null; 
		
		//yelp.search() takes two inputs
		//System.out.println(yelp.search("Chinese food", "Kansas City, MO"));
		
		//Criteria
		String[] kinds = {item};
		//String[] cities = {"Kansas City, MO", "Blacksberg, VA", "Atlanta, GA"}; //OFFLINE
		String[] cities = {location}; //ONLINE
		
		for(String city: cities)
			for(String kind: kinds)
			{
				businesses = yelp.search(kind, city);
				/*
				 for(Business b: businesses)
						System.out.println(b.getName() + " " + b.getRating());*/
			}
		return businesses;
	}
	
	/**
	 * Creates a JFrame that displays GUI for the program
	 */
	public void setUp()
	{
		JFrame f = new JFrame("Eateries Search");
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0,0,700,500);
		panel.setBackground(Color.blue);
		
		JLabel title = new JLabel("Welcome to the Eateries Search");
		title.setFont(new Font("Serif", Font.BOLD, 40));
		title.setForeground(Color.white);
		title.setBounds(80,1,700,100);
		
		JLabel des = new JLabel("Enter a food or item and then enter a location."); 
        des.setFont(new Font("Serif", Font.BOLD, 15));
        des.setForeground(Color.white); 
        des.setBounds(200,70,500,50); 
        
        JLabel des1 = new JLabel("Ex: 'cleaning supplies','London, Enlgand' or 'Chinese food','San Diego, CA'"); 
        des1.setFont(new Font("Serif", Font.BOLD, 15));
        des1.setForeground(Color.white); 
        des1.setBounds(110,90,500,50); 
        
        JLabel lab = new JLabel("Enter a food or item: "); 
        lab.setFont(new Font("Serif", Font.BOLD, 15));
        lab.setForeground(Color.white); 
        lab.setBounds(50,150,170,25);
        
        tf = new JTextField();  
        tf.setBounds(200,150,100,30); 
        
        JLabel lab1 = new JLabel("Enter a location: "); 
        lab1.setFont(new Font("Serif", Font.BOLD, 15));
        lab1.setForeground(Color.white); 
        lab1.setBounds(50,210,170,25);
        
        tf1 = new JTextField();  
        tf1.setBounds(170,210,100,30); 
        
        b1 = new JButton("Search");
        b1.setBounds(110,270,100,30);
        
        textArea = new JTextArea();  
        JScrollPane scroll = new JScrollPane(textArea); 
        textArea.setBounds(360,150,300,300);  
        textArea.setEditable(false); 
        
        JLabel lab2 = new JLabel("Top 5 Rated Businesses"); 
        lab2.setFont(new Font("Serif", Font.BOLD, 15));
        lab2.setForeground(Color.white); 
        lab2.setBounds(20,305,170,25);
        
        ta1 = new JTextArea();
        ta1.setBounds(20, 330, 300, 90);
        ta1.setEditable(false);
        
        b1.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                String s1=tf.getText();  
                String s2=tf1.getText();
                
                output = onlineInfo(s1,s2);
                
                for(Business b: output)
                {
                	textArea.append(b.getName() + " " + b.getRating() + "\n"); 
                }
                
                selectionSort(output, output.size());
                for(int i = output.size() - 5; i < output.size(); i++)
                {
                	ta1.append(output.get(i).getName() + " " + output.get(i).getRating() + "\n");
                }
            }
        }); 
        
        panel.add(title);
        panel.add(des);
        panel.add(lab);
        panel.add(lab1);
        panel.add(des1);
        panel.add(tf);
        panel.add(tf1);
        panel.add(b1);
        panel.add(textArea);
        panel.add(scroll);
        panel.add(ta1);
        panel.add(lab2);
        
        f.add(panel);
        f.setSize(700,500);
        f.setLayout(null);
        f.setVisible(true);
	}

}
